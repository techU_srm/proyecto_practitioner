//importaciones  de dependencias
var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');//estaba comentado y faltaba instalar dependencia request-json
var morgan      = require('morgan'); // es una libreria para logar las request http para que ayude a depurar
var ig = require("iban-generator");// es la librería para generar iban con el cambio 0182
var jwt = require('jsonwebtoken'); //para usar jwt
var expressJwt = require('express-jwt'); //para usar jwt
var uuidv1 = require('uuid/v1'); //cálculo de identificadores únicos
var config = require('./config');//variables de configuracion mejor en archivo separado
var https = require('https');//para hablitar conexion SSL
var fs = require('fs');//para leer archivos del filesystem

var cors = require('cors');//para hacer peticiones cors sin tener el plugin de chrome

var jwtClave = config.jwtClave; //clave que usa express-jwt para cifrar el token
var baseMLabURL = config.baseMLabURL;//la direccion de mlab
var mLabAPIKey = config.mLabAPIKey; //apikey de mlab
//var port = process.env.PORT || 3000; //el puerto para escuchar
var port = config.port; //el puerto para escuchar http
var sslPort = config.sslPort; //el puerto para escuchar https

// La info sobre el certificado y la clave que se usará para servir por SSL
var sslOptions = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem'),
  passphrase: jwtClave
};

var app = express();


// Revisa el scope
function requireScope(scope) {
  return function (req, res, next) {
    var has_scopes = req.user.scope === scope;
    if (!has_scopes) {
        res.sendStatus(401);
        return;
    }
    next();
  };
}


//bloqueo de todos los path/endpoints salvo los que usamos para logar o registrar un usuario

app.use(expressJwt({secret:jwtClave}).unless({method: 'OPTIONS' ,path: [
  {
    url:'/apicaras/login',
    methods: 'POST'
  },
  {
    url: '/apicaras/users',
    methods: 'POST'
  }
]}));

//pruebas para el usuario admin
//app.use('/api/protected', jwtCheck, requireScope('full_access'));//por si hago funciones de admin
//app.use('/api/protected', jwtCheck, requireScope('normal_access'));

app.use(morgan('dev'));// usar morgan para que se logen las requests a la consola
app.use(bodyParser.json()); //permite parsear el body

//app.use(bodyParser.urlencoded({ extended: true }));//por si fuera necesario para parsear info que venga por cabeceras
//para el jwt ya lo hace expressJwt

app.use(cors());//para hacer peticiones cors sin tner el plugin de chrome

//crea un servidor que escucha por SSL en el puerto 3443
https.createServer(sslOptions, app).listen(sslPort);
console.log("API escuchando https en el puerto " + sslPort);
app.listen(port);//lanza el servidor para que escuche
console.log("API escuchando en el puerto " + port);


//console.log(uuidv1());

//funcion que crea token de acceso
function creaAccessToken(user) {
  return jwt.sign({
    iss: config.issuer,
    aud: config.audience,
    exp: Math.floor(Date.now() / 1000) + (60*60),
    scope: 'full_access',
    sub: user,
    jti: uuidv1(), // unique identifier for the token
    alg: 'HS256'
  }, config.jwtClave);
}
//console.log(creaAccessToken("carlos@gmail.com"));



function obtenerFechaHoraFormateada(){
  date=new Date();
  var fecha = date.getDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear();
  var hora = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
  var fechaYHora = fecha + '    ' + hora;
  return(fechaYHora);
}

//FUNCIÓN que devuelve un IBAN
function crearCuenta() {
  var iban;
  aleatorio = ig.randomNumber();
  console.log(aleatorio);
  ccc=ig.fixCCC(aleatorio);
  console.log(ccc);
  iban=ig.doIban(ccc);
  console.log(iban);
  //resultado = ig.checkIban("ES1398610951768815578473")
  resultado = ig.checkIban(iban);
  console.log(resultado);
    return iban;
}

//MÉTODO ADMIN
//LISTAR USUARIOS
app.get("/apicaras/users",
  function(req, res){
    console.log(" GET /apicaras/users");
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");
    httpClient.get("users?" + mLabAPIKey,
        function(err,resMLab,body){
          var response =!err ? body : {
            "msg" : "Error obteniendo usuarios"
          }
          res.send(response);
        })
  }
);

//MOSTRAR USUARIO.
app.get("/apicaras/users/:uid",
  function(req, res){
    console.log(" GET /apicaras/users/:uid");
    var uid = req.params.uid;
    var query = 'q={"uid":' + uid+'}';
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");
    httpClient.get("users?"+ query + "&" + mLabAPIKey,
        function(err,resMLab,body){
        var response = {};
        if (err){
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        }else{
          if (body.length > 0){
            response=body;
          }else{
            response = {
              "msg": "Usuario no encontrado"
            };
            res.status(404);
          }
        }
          res.send(response);
        })
  }//funtion(req, res)
);

function validarEmail(valor) {
  if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(valor)){
   console.log("La dirección de email " + valor + " es correcta.");
   return (true);
  } else {
   console.log("La dirección de email es incorrecta.");
   return(false);
  }
}

function validarNombre(valor) {
  if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
    console.log("El nombre " + valor + " es incorrecto.");
    return false;
  }else{
    return true
  }
}

function validarContraseña(valor) {
  if (valor != null ||/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(valor)){
   console.log("La contraseña " + valor + " es correcta.");
   return (true);
  } else {
   console.log("La contraseña es incorrecta.");
   return(false);
  }
}

//FUNCIÓN que devuelve el índice mayor +1 de la colección que le pasamos por parámetro
// la colleción será users o accounts, y devolverá el uid o aid respectivamente
function calculaIndice(collection, indice, callback){
  var httpClient = requestJson.createClient(baseMLabURL);
  var query= "?f={"+indice + " : 1}&s={"+indice+":1}&";
  httpClient.get(collection + query + mLabAPIKey,
  function(err,resMLab,body){
  var long=body.length;
  if (err){
          console.log("Error accediendo a mLab");
      }else{
        if (long > 0){
          if (collection == "users"){
            //console.log("encuentro coleccion")
             return callback(body[long-1].uid+1);
          }else{
            return callback(body[long-1].aid+1);
          }
          //console.log("es mayor que 0");
        }else{
          console.log("devuelve un 1");
          return callback(1);
      }
      }
  })
}

//CREAR USUARIO
app.post("/apicaras/users",
  function(req, res){
  console.log(" POST /apicaras/users");

  var emailCorrecto = validarEmail(req.body.email);
  var nombreCorrecto = validarNombre(req.body.first_name);
  var contraseñaCorrecta = validarContraseña(req.body.password);
  if (!emailCorrecto){
    response = {
      "msg" : "Email incorrecto"
    }
    res.status(400);
    res.send(response);
  }
  else if(!nombreCorrecto){
   response = {
     "msg" : "Nombre incorrecto"
   }
   res.status(400);
   res.send(response);
  }else if(!contraseñaCorrecta){
   response = {
     "msg" : "Contraseña incorrecta"
   }
   res.status(400);
   res.send(response);

  }else{

     uid = calculaIndice("users", "uid", function(uid){
         var postBody = {
            "uid" : uid,
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "password" : req.body.password
         };
         console.log(uid);
         var httpClient = requestJson.createClient(baseMLabURL);
         console.log("Cliente HTTP creado");
         httpClient.post("users?" + mLabAPIKey, postBody,
             function(err,resMLab,body){
               var response = {};
               if (err){
                 var response = {
                   "msg" : "Error creando usuario"
                 }
                 res.status(500);
               }else{
                 response = {
                   "msg" : "Usuario creado",
                   "uid" : uid,
                   "first_name" : req.body.first_name,
                   "last_name" : req.body.last_name,
                   "email" : req.body.email,
                   "access_token": creaAccessToken(req.body.email),
                   "id_token": "123456"
                   };
                   res.status(200);
               }
               //console.log("response " + JSON.stringify(response));
               res.send(response);
             })
           }
         ); //calculaIndice
    } //else if
  }
);


//MODIFICAR PERFIL DE USUARIO
app.put("/apicaras/users/:uid",
  function(req, res){
  console.log(" PUT /apicaras/users/:uid");
      var emailCorrecto = validarEmail(req.body.email);
      var nombreCorrecto = validarNombre(req.body.first_name);
      var contraseñaCorrecta = validarContraseña(req.body.password);
      if (!emailCorrecto){
        response = {
          "msg" : "Email incorrecto"
        }
        res.status(400);
        res.send(response);
      }
      else if(!nombreCorrecto){
       response = {
         "msg" : "Nombre incorrecto"
       }
       res.status(400);
       res.send(response);
      }else if(!contraseñaCorrecta){
       response = {
         "msg" : "Contraseña incorrecta"
       }
       res.status(400);
       res.send(response);
     }else{//una vez comprobados los formatos de las entradas
       var uid = req.params.uid;
       var putBody = {
          "uid" : uid,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
       };
       var query = 'q={"uid":' + uid+'}';
       var httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente HTTP creado");

       httpClient.put("users?" + query + "&" + mLabAPIKey, putBody,
           function(err,resMLab,body){
             var response={};
             if (err) {
               response = {
               "msg" : "Error modificando usuarios"
               }
               res.status(500);
             }else{
               console.log(body);
               console.log("pintado");
               response = {
               "msg": "Usuario modificado",
               "uid": uid,
               "access_token": creaAccessToken(req.body.email),
               "id_token": "123456"
                }
                res.status(200);
              }
             res.send(response);
        })//httpClient.put
     }//else
     //res.send(response);
});


//BORRAR USUARIO. da un fallo en MLab
app.delete("/apicaras/users/:uid",
  function(req, res){
    console.log(" DELETE /apicaras/users/:uid");
    var uid = req.params.uid;
    console.log(req.params.uid);
    var query = 'q={"uid":' + uid+ '}';
    console.log("query :" ,query)
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");
      httpClient.delete("users?"+ query + "&" + mLabAPIKey,
        function(err,resMLab,body){
          var response = {};
          if (err){
            response = {
              "msg" : "Error borrando usuario"
            }
            res.status(500);
          }else{
            response = {
              "msg" : "Usuario borrado"
            }
          }
          console.log("resMlab.body :",resMLab.body);
            res.send(response);
          })
  }
);

//LISTAR CUENTAS
app.get("/apicaras/users/:uid/accounts",
  function(req, res){
    console.log(" GET /apicaras/users/:uid/accounts");
    var uid = req.params.uid;
    var query = 'q={"uid":' + uid + '}';
    //console.log("query", query);
    //console.log("uid", uid);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("accounts?"+ query + "&" + mLabAPIKey,
        function(err,resMLab,body){
        var response = {};
        if (err){
          var response = {
            "msg" : "Error obteniendo cuentas de usuario"
          }
          res.status(500);
        }else{
          if (body.length > 0){
            response=body;
          }else{
            response = {
              "msg": "El usuario no tiene cuentas"
            };
            res.status(204);
          }
        }
          console.log(response);
          res.send(response);
        })

  }//funtion(req, res)
);

//MOSTRAR DETALLE CUENTA
app.get("/apicaras/users/:uid/accounts/:aid",
  function(req, res){
    console.log(" GET /apicaras/users/:uid/accounts/:aid");
    var uid = req.params.uid;
    var aid = req.params.aid;
    var query = 'q={"uid":' + uid + ',"aid": ' +aid +'}';
    console.log("query", query);
    console.log("uid", uid);
    console.log("aid", aid);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");
    httpClient.get("accounts?"+ query + "&" + mLabAPIKey,
        function(err,resMLab,body){
        var response = {};
        if (err){
          var response = {
            "msg" : "Error obteniendo cuenta de usuario"
          }
          res.status(500);
        }else{
          if (body.length > 0){
            response=body;
          }else{
            response = {
              "msg": "El usuario no tiene esa cuenta"
            };
            res.status(404);
          }
        }
          res.send(response);
        })

  }//funtion(req, res)
);

//CREAR CUENTA: no comprueba, por el momento que el usuario existe
app.post("/apicaras/users/:uid/accounts",
  function(req, res){
    console.log(" POST /apicaras/users/:uid/accounts");
    var aid = calculaIndice("accounts", "aid", function(aid){
    var uid=parseInt(req.params.uid);
    var iban= crearCuenta();
    var postBody = {
       "uid" : uid,
       "aid" : aid,
       "IBAN" : iban,
       "saldo" : 0.00,
     };
     console.log(req.params.uid);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.post("accounts?" + mLabAPIKey, postBody,
        function(err,resMLab,body){
          var response =!err ? body : {
            "msg" : "Error creando cuenta"
          }
          console.log(resMLab.body);
          res.send(body);
        })
      })
  }
);

//BORRAR CUENTA: no funcione, seguir después con ella
//Probé con put sin existo
app.put("/apicaras/users/:uid/accounts/:aid",
//parte de express
  function(req, res){
    console.log(" PUT /apicaras/users/:uid/accounts/:aid");
    //var deleteBody = {
    //   "aid" : req.params.aid,
    // };
    var uid = req.params.uid;
     var aid = req.params.aid;
     console.log(req.params.aid);
     var query = 'q={"uid":' + uid + ',"aid": ' +aid +'}';
     console.log("query :" ,query)
     var httpClient = requestJson.createClient(baseMLabURL);
     console.log("Cliente HTTP creado");
       httpClient.delete("accounts?"+ query + "&" + mLabAPIKey,
        function(err,resMLab,body){
          var response =!err ? body : {
            "msg" : "Error borrando cuenta"
          }
          console.log(resMLab.body);
          res.send(body);
        })
  }
);


//LISTAR MOVIMIENTOS
app.get("/apicaras/users/:uid/accounts/:aid/movements",
  function(req, res){
    console.log(" GET /apicaras/users/:uid/accounts/:aid/movements");
    var uid = req.params.uid;
    var aid = req.params.aid;
    var query = 'q={"uid":' + uid + ',"aid": ' +aid +'}';
    console.log("query", query);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");
    httpClient.get("movements?"+ query + "&" + mLabAPIKey,
        function(err,resMLab,body){
          var response = {};
          if (err){
            var response = {
              "msg" : "Error obteniendo movimientos de usuario"
            }
            res.status(500);
          }else{
            if (body.length > 0){
              response=body;
            }else{
              response = {
                "msg": "El usuario no tiene movimientos en esta cuenta"
              };
              res.status(204);
            }
          }
          res.send(response);
        //  console.log("he respondido " + JSON.stringify(response));
        }
    )
  }//funtion(req, res)
);

//CREAR MOVIMIENTO
app.post("/apicaras/users/:uid/accounts/:aid/movements",
  function(req, res){
    console.log(" POST /apicaras/users/:uid/accounts/:aid/movements");
    var uid=parseInt(req.params.uid);
    var aid=parseInt(req.params.aid);
    var iban=req.body.IBAN;
    var importe=parseFloat(req.body.importe);
    var tipo=req.body.tipo;
    var fecha= obtenerFechaHoraFormateada();

    // si se ha pasado algo por el body se coge y si no se me mete la del día
      //  if ((req.body.fecha==null)||(req.body.fecha.length==0)) {
      //    var fecha=req.body.fecha;
      //  }else{
      //    var fecha= new Date();
      //  }
    var postBody = {
       "uid" : uid,
       "aid" : aid,
       "IBAN" : iban,
       "importe" : importe,
       "tipo" : tipo,
       "fecha" : fecha
     };

     var httpClient = requestJson.createClient(baseMLabURL);
     console.log("Cliente HTTP creado");
     console.log("antes del post ");
     console.log(iban);
     httpClient.post("movements?" + mLabAPIKey, postBody,
        function(err,resMLab,body){
          console.log("aqui1");
          var response = {};
          if (err){
            var response = {
              "msg" : "Error creando movimiento"
            }
            res.status(500);
            res.send(response);
          }else{//Ha creado el movimiento
            //leo el balance de la cuenta aid
            var queryAccount = 'q={"uid":' + uid + ',"aid": ' +aid +'}';

            httpClient.get("accounts?"+ queryAccount + "&" + mLabAPIKey,
               function(errGet,resMLabGet,bodyGet){
                 var response = {};
                 if (err){
                   var response = {
                     "msg" : "Error leyendo el balance de la cuenta"
                   }
                   res.status(500);
                   res.send(response);
                 }else{
                   var saldoAnterior = parseFloat(bodyGet[0].saldo);

                   var saldo = 0;
                   if (tipo=="I"){
                     saldo = saldoAnterior + importe;
                   }else{
                     saldo = saldoAnterior - importe;
                   }
                  console.log("saldo ");
                  console.log(saldo);

                   var putBody ='{"$set":{"saldo":'+ saldo + '}}';

                   //actualiza balance
                   console.log(putBody);
                   console.log(queryAccount);
                   console.log("antes del cd .put");
                    httpClient.put("accounts?" + queryAccount +"&"+ mLabAPIKey, JSON.parse(putBody),
                     function(errPut,resMLabPut,bodyPut){
                       if (errPut){
                         var response = {
                           "msg" : "Error actualizando balance"
                         }
                         res.status(500);
                         res.send(response);
                       }
                    })//httpClient.put
                   }//else
            })//httpClient.get
            response = {
              "msg" : "Movimiento creado correctamente"
              };
            res.status(200);
            res.send(response);
          }//else
      })//httpClient.post
})

//LOGIN USUARIO a lo mejor hay que cambiarlo por un get
app.post("/apicaras/login",
  function(req,res){
    //Filtramos si el body no lleva usuario y password
    if (req.body.email && req.body.password ){
      console.log(" POST /apicaras/login");
      var email = req.body.email;
      var password = req.body.password;
      console.log("email : " + email);
      console.log("password : " + password);
      var query = 'q={"email":"' + email + '", "password":"' + password +'"}';
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("users?" + query + "&" + mLabAPIKey);
      httpClient.get("users?" + query + "&" + mLabAPIKey,
        function(err,resMLab,body){
          var response = {};
          if (err){
            var response = {
              "msg" : "Error obteniendo usuario"
            }
            res.status(500);
          }else{
            if (body.length > 0){
              response = {
                "msg": "Usuario logado",
                "uid": body[0].uid,
                "first_name": body[0].first_name,
                "last_name" : body[0].last_name,
                "email" : body[0].email,
                //"access_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IiBTYXJhIFJlcXVlam8iLCJpYXQiOjE1MTYyMzkwMjJ9.htbjVqxRbi3GnFpUKiUp9gADZp65_ASG8xxrhCfJs7Q",
                "access_token": creaAccessToken(email),
                "id_token": "123456"
              };
              res.status(200);
              res.send(response);
            }else{ //podríamos dar errores distintos diferenciando si no est´a el usuario o password incorrecta
              response = {
                "msg": "Credenciales incorrectas"
                };
              res.status(404);
              res.send(response);
            }
          }
        }
      )
    }else{
      //console.log("no existe el ");
      response = {
        "msg": "Hay que proporcionar un usuario y un password"
        };
      //Mala petición
      res.status(400);
      res.send(response);
    }
  }
);

//LOGOUT USUARIO
//Por ahora sin uso en el backend - habría que expirar el token
app.post("/apicaras/logout",
function(req,res){
  console.log("POST /apicaras/logout");

  var uid = req.body.uid;
  var query = 'q={"uid":' + uid + '}';
  var httpClient = requestJson.createClient(baseMLabURL);
  //console.log("users?" + query + "&" + mLabAPIKey);
httpClient.get("users?" + query + "&" + mLabAPIKey,
  function(err,resMLab,body){
    //var userId = body[0].uid
    //console.log(uid);
    var response = {};
    if (err){
      var response = {
        "msg" : "Error obteniendo usuario"
      }
      res.status(500);
    }else{
      if (body.length > 0){
        var putBody = '{"$unset":{"logged":true}}';
        var query2 = 'q={"uid":' + uid + '}';
        if (body[0].logged==true){
          console.log("body[0].logged=true" + body[0].logged);
            httpClient.put("users?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody),
              function(errput,resMLabput,bodyput){
                console.log("user?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody));
                console.log(body[0]);
              response = {
                "msg": "Logout correcto", "uid": uid
              };
              res.send(response);
              //console.log("Entro en el put");
              }
            )
          }
          else{
            //  if (body[0].logged=true)
            response = {
              "msg": "El usuario no está logado"
            }
            res.send(response);
          }
      }else{
        response = {
          "msg": "Usuario no encontrado"
        };
        res.status(404);
        res.send(response);
      }
    }
  }
)
}
)
